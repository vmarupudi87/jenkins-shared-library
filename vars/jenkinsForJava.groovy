// jenkinsForJava.groovy
def call(String javaVersion, String mavenVersion) {
  pipeline {
       agent any
       tools {
           maven "${mavenVersion}"
           jdk "${javaVersion}"
       }
       stages {
           stage("Tools initialization") {
               steps {
                   sh "mvn --version"
                   sh "java -version"
               }
           }

           stage("Cleaning workspace") {
               steps {
                   sh "mvn clean"
               }
           }
           stage("Running Testcase") {
           
               when {
           
                   beforeAgent true
               
                   expression {
               
                       env.SOMETHING = 'TEST'
                
                    }
          
               }
                  steps {
                   
                   sh "mvn test"
                  
                  }
           }
           stage("Packing Application") {
               steps {
                   sh "mvn -T 10 clean package -DskipTests"
               }
           }
       }
   }
}